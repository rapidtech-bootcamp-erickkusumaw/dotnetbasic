﻿using System;
namespace Polymorphism
{
    public class MainDataType
    {
        public static void Main()
        {
            SamplePolymorphism();
        }

        #region Polimorphism
        public static void SamplePolymorphism()
        {
            var shapes = new List<Shape>

            {

                new Rectangle(),
                new Triangle(),
                new Circle()

            };

            foreach (var shape in shapes)
            {

                shape.Draw();

            }
        }
        #endregion
    }
}

