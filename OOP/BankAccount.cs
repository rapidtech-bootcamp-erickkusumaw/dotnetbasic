using System;
namespace OOP
{

    public class BankAccount
    {

        private static int accountNumberSeed = 1234567890;

        // Read
        public string Number { get; }

        // Read and Write
        public string Owner { get; set; }

        // Read
        public decimal Balance
        {
            get
            {
                decimal balance = 0;
                foreach (var item in allTransactions)
                {
                    balance += item.Amount;
                }

                return balance;
            }
        }


        public BankAccount()
        {

        }

        public BankAccount(string name, decimal initialBalance)
        {

            Number = accountNumberSeed.ToString();
            accountNumberSeed++;

            Owner = name;
            MakeDeposit(initialBalance, DateTime.Now, "Initial Balance");

        }

        private List<Transaction> allTransactions = new List<Transaction>();

        public void MakeDeposit(decimal amount, DateTime date, string note)
        {

            if (amount <= 0)
            {

                throw new ArgumentOutOfRangeException(nameof(amount), "Amount deposite be positif");

            }

            var deposit = new Transaction(amount, date, note);
            allTransactions.Add(deposit);

        }


        public void MakeWithdrawal(decimal amount, DateTime date, string note)
        {

            if (amount <= 0)
            {

                throw new ArgumentOutOfRangeException(nameof(amount), "Not funds for this Withdraw");

            }
            if (Balance - amount < 0)
            {

                throw new InvalidOperationException("Not funds for this Withdraw");

            }

            var withdrawal = new Transaction(-amount, date, note);
            allTransactions.Add(withdrawal);

        }
    }

}







