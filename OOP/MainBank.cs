﻿using System;
namespace OOP
{

    public class MainBank
    {

        public static void Main()
        {
            // BankAccountSample();
            TransactionBankAccount();
        }

        public static void TransactionBankAccount()
        {

            var account = new BankAccount("Bery", 1000);
            Console.WriteLine($"Account {account.Number} was created for {account.Owner} with {account.Balance} initial balance");

            // Test for a negative balance
            try
            {

                account.MakeWithdrawal(750, DateTime.Now, "Attempted to Overdraw");

            }
            catch (InvalidOperationException e)
            {

                Console.WriteLine("Exception cought trying to overdraw");
                Console.WriteLine(e.ToString());

            }

            try
            {

                account.MakeWithdrawal(750, DateTime.Now, "Attempted to Overdraw");

            }
            catch (InvalidOperationException e)
            {

                Console.WriteLine("Exception cought trying to overdraw");
                Console.WriteLine(e.ToString());

            }


            // Test initial balacne must be positive
            BankAccount invalidAccount;
            try
            {

                invalidAccount = new BankAccount("Invalid", -55);

            }
            catch (ArgumentOutOfRangeException e)
            {

                Console.WriteLine("Exception cought creating account with negative balance");
                Console.WriteLine(e.ToString());
                return;

            }

        }

        #region Sample BankAccount
        public static void BankAccountSample()
        {

            var account = new BankAccount("Denjaka", 100000000);
            Console.WriteLine($"Account {account.Number} was created for {account.Owner} with {account.Balance} initial balance");

        }
        #endregion

    }
}