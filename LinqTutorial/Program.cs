﻿using LinqTutorial.Model;

namespace LinqTutorial;

public class ProgramMain
{
    public static void Main()
    {

        Product.SampleFilterProduct();
        
    }

    #region Introduction
    public static void IntroductionLinq()
    {

        Console.WriteLine("Introduction without LinQ : ");
        IntroLinq.Introduction();

        Console.WriteLine("\n Intro with Linq");
        IntroLinq.IntroductionWithLinq();

    }
    #endregion
}
