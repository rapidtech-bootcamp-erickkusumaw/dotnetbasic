using System;
namespace LogicDasar
{
	public class Logic02Soal03
	{
		public Logic02Soal03()
		{
		}

		public static void CetakData(int n)
		{
			for (int i = 0; i < n; i++)
			{
				int angka = 1;
				for (int j = 0; j < n; j++){
					if(i == 0){
						Console.Write(angka +"\t");
					} else if(j == 0){
                        Console.Write(angka);
                    } else if(j == n - 1 || i == n - 1){
						Console.Write(angka +"\t");
					}
					else{
						Console.Write("\t");
					}

					angka += 2;
				}
                Console.WriteLine("\n");
            }
		}
	}
}
